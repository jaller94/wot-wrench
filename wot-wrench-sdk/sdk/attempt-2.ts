type Callback = (data: unknown) => void;

type WSConnection = {
    url: string,
    webSocket?: WebSocket,
    propertyCallbacks: {
        thingId: string,
        propertyName: string,
        callback: Callback,
    }[],
    activePropertyCallbacks: Set<{
        thingId: string,
        propertyName: string,
        callback: Callback,
    }>,
};

const connections = new Map<string, WSConnection>();

const connect = (connection: WSConnection): void => {
    connection.activePropertyCallbacks = new Set();
    connection.webSocket = new WebSocket(connection.url);
    connection.webSocket.addEventListener('open', event => {
        console.debug('ws open');
        updatePropertyObservations(connection);
    });
    connection.webSocket.addEventListener('message', event => {
        if (connection === undefined) {
            return;
        }
        console.debug('ws message', event);
        const msg = JSON.parse(event.data);
        if (msg.messageType === 'propertyReading') {
            for (const callback of connection.propertyCallbacks) {
                if (msg.thingId === callback.thingId && msg.property === callback.propertyName) {
                    callback.callback(msg.data);
                }
            }
        }
    });
    connection.webSocket.addEventListener('close', event => {
        console.debug('ws close', event);
        // Just to make TypeScript happy
        if (!connection) {
            return;
        }
        if (!paused) {
            return;
        }
        if (connection.propertyCallbacks.length !== 0) {
            connect(connection);
        }
    });
    connection.webSocket.addEventListener('error', event => {
        console.error('ws error', event);
        // Just to make TypeScript happy
        if (!connection) {
            return;
        }
        if (!paused) {
            return;
        }
        if (connection.propertyCallbacks.length !== 0) {
            connect(connection);
        }
    });
};

const disconnect = (connection: WSConnection): void => {
    connection.activePropertyCallbacks = new Set();
    if (connection.webSocket) {
        connection.webSocket.close();
        connection.webSocket = undefined;
    }
}

const getOrCreateConnection = (url: string): WSConnection => {
    let c = connections.get(url);
    if (!c) {
        c = {
            url,
            propertyCallbacks: [],
            activePropertyCallbacks: new Set(),
        } satisfies WSConnection;
        connect(c);
        connections.set(url, c);
    }
    return c;
}

const updatePropertyObservations = (connection: WSConnection): void => {
    if (paused) {
        return;
    }
    if (!connection.webSocket) {
        console.error('updatePropertyObservations failed because .webSocket was not defined');
        return;
    }
    if (connection.webSocket.readyState !== WebSocket.OPEN) {
        console.warn('updatePropertyObservations failed because .webSocket is not open');
        return;
    }
    for (const callback of connection.propertyCallbacks) {
        if (!connection.activePropertyCallbacks.has(callback)) {
            connection.webSocket.send(JSON.stringify({
                messageType: 'observeProperty',
                thingId: callback.thingId,
                property: callback.propertyName,
            }));
            connection.webSocket.send(JSON.stringify({
                messageType: 'readProperty',
                thingId: callback.thingId,
                property: callback.propertyName,
            }));
            // TODO: Listen for errors
            connection.activePropertyCallbacks.add(callback);
        }
    }

    // Cleanup
    for (const callback of connection.activePropertyCallbacks) {
        if (!connection.propertyCallbacks.includes(callback)) {
            connection.webSocket.send(JSON.stringify({
                messageType: 'unobserveProperty',
                thingId: callback.thingId,
                property: callback.propertyName,
            }));
            connection.activePropertyCallbacks.delete(callback);
        }
    }
}

export const addPropertyListener = (url: string, thingId: string, propertyName: string, callback: Callback): void => {
    console.debug('addPropertyListener', url, thingId, propertyName);
    const connection = getOrCreateConnection(url);
    connection.propertyCallbacks.push({
        thingId,
        propertyName,
        callback,
    });
    updatePropertyObservations(connection);
}

export const removePropertyListener = (url: string, thingId: string, propertyName: string, callback: Callback): void => {
    console.debug('addPropertyListener', url, thingId, propertyName);
    const connection = getOrCreateConnection(url);
    connection.propertyCallbacks = connection.propertyCallbacks.filter((v) => v.thingId !== thingId && v.propertyName !== propertyName && v.callback !== callback);
    updatePropertyObservations(connection);
}

let paused = false;

export const pause = (): void => {
    if (paused) {
        return;
    }
    for (const connection of connections.values()) {
        disconnect(connection);
    }
    paused = true;
}

export const unpause = (): void => {
    if (!paused) {
        return;
    }
    for (const connection of connections.values()) {
        connect(connection);
    }
    paused = false;
}
