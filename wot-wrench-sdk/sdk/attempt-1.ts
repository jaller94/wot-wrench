type Callback = (properties: Record<string, unknown>) => void;

type Thing = {
    description: Promise<unknown>,
    listeners: Set<Callback>,
    propertyValues?: Record<string, any>,
    websocket?: WebSocket,
};

const things: Map<string, Thing> = new Map();

async function getDescription(url: string) {
    const res = await fetch(url);
    if (!res.ok) {
        throw new Error(`HTTP error ${res.status}: ${res.statusText}`);
    }
    return res.json();
}

function createWebSocket(thing: Thing, url: string, propertyList: Set<string>) {
    const webSocket = new WebSocket(url);
    const data: Record<string, true> = {};
    for (const propertyKey of propertyList) {
        data[propertyKey] = true;
    }
    webSocket.addEventListener('open', event => {
        webSocket.send(JSON.stringify({
            messageType: 'addEventSubscription',
            data,
        }));
        console.debug('ws open');
    });
    webSocket.addEventListener('message', (event) => {
        console.debug('ws message', event);
        const msg = JSON.parse(event.data);
        console.debug(msg);
        if (msg.messageType === 'propertyStatus' && typeof msg.data === 'object') {
            thing.propertyValues = msg.data;
        }
    });
    webSocket.addEventListener('close', (event) => {
        console.debug('ws close', event);
    });
    webSocket.addEventListener('error', (event) => {
        console.error('ws error', event);
    });
    return webSocket;
}

export function addPropertiesListener(url: string, callback: Callback) {
    let thing = things.get(url);
    if (!thing) {
        thing = {
            description: getDescription(url),
            listeners: new Set(),
        };
        things.set(url, thing);
    }
    thing.listeners.add(callback);
    getDescription(url).then()
}

export function removePropertiesListener(url: string, callback: Callback) {
    let thing = things.get(url);
    if (!thing) {
        return;
    }
    thing.listeners.delete(callback);
}
