import React from 'react';
import { useFetch } from '../single/useFetch';

export function useThing(thingUrl: string): [boolean, string, null | Record<string, unknown>] {
    const [thingLoading, thingError, thingJson] = useFetch(thingUrl); 

    let thingObj = null;
    let error = '';
    try {
        thingObj = JSON.parse(thingJson);
    } catch {
        error = 'Invalid JSON';
    }

    return [
        thingLoading,
        thingError || error,
        thingObj,
    ];
}