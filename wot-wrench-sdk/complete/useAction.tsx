import React, { useCallback, useMemo } from 'react';
import z from 'zod';
import { useFetch } from '../single/useFetch';
import { useTDToActionAffordance } from '../single/useTDToAffordances';

const zEventAffordance = z.object({
    forms: z.array(z.object({
        contentType: z.string().optional(),
        href: z.string(),
        'htv:methodName': z.string().optional(),
        op: z.union([z.string(), z.array(z.string())]).optional(),
    })).optional(),
});

export function useAction(thingUrl: string, actionName: string): [
    actionAffordance: unknown,
    invoke?: (obj?: Record<string, unknown>) => Promise<unknown>,
] {
    const [thingLoading, thingError, thingJson] = useFetch(thingUrl);
    const [actionError, actionAffordanceJson] = useTDToActionAffordance(thingJson, actionName);
    const actionDescription: unknown = useMemo(() => {
        return actionAffordanceJson.startsWith('{') ? JSON.parse(actionAffordanceJson) : null;
    }, [actionAffordanceJson]);
    const safeActionAffordance = zEventAffordance.safeParse(actionDescription);
    const invokeActionForm = useMemo(() => {
        return safeActionAffordance.data?.forms?.find(form => (
            typeof form.href === 'string' &&
            (Array.isArray(form.op) ? form.op.includes('invokeaction') : form.op === 'invokeaction') &&
            (form['htv:methodName'] === undefined || typeof form['htv:methodName'] === 'string') &&
            (form.contentType === undefined || (typeof form.contentType === 'string' && form.contentType.startsWith('application/json')))
        ));
    }, [actionDescription]);

    const invokeAction = useMemo(() => {
        if (!invokeActionForm) {
            return;
        }
        return async (data: unknown = {}): Promise<unknown> => {
            const res = await fetch(invokeActionForm.href, {
                method: invokeActionForm['htv:methodName'] ?? 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            });
            if (!res.ok) {
                throw Error(`HTTP Error ${res.status}: ${res.statusText}`);
            }
            try {
                return await res.json();
            } catch (err) {
                console.warn('Failed to parse action response', err);
                return;
            }
        }
    }, [invokeActionForm]);

    return [
        actionDescription,
        invokeAction,
    ];
}