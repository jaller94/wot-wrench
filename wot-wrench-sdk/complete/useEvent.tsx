import React, { useMemo } from 'react';
import z from 'zod';
import { useFetch } from '../single/useFetch';
import { useTDToEventAffordance } from '../single/useTDToAffordances';
import { useJSON } from '../single/useJSON';

let swRegistration: ServiceWorkerRegistration | null = null;
        
// Check if service workers and push messaging is supported
if ('serviceWorker' in navigator && 'PushManager' in window) {
    // Register service worker
    navigator.serviceWorker.register('serviceworker.js')
        .then(function(registration) {
            console.log('Service Worker registered');
            swRegistration = registration;
            
            // Check if already subscribed
            return swRegistration.pushManager.getSubscription();
        })
        .then(function(subscription) {
            if (subscription) {
                console.log('Already subscribed');
            }
        })
        .catch(function(error) {
            console.error('Service Worker Error', error);
        });
}


function hasOp(ops: unknown, op: string) {
    return Array.isArray(ops) ? ops.includes(op) : ops === op;
}

const zThing = z.object({
    base: z.string().optional(),
});

const zEventAffordance = z.object({
    forms: z.array(z.object({
        contentType: z.string().optional(),
        href: z.string(),
        'htv:methodName': z.string().optional(),
        op: z.union([z.string(), z.array(z.string())]).optional(),
        vapidPublicKey: z.string().optional(),
    })).optional(),
});

export function useEvent(thingUrl: string, eventName: string): [
    eventDescription: unknown,
    subscribe?: (data?: unknown) => Promise<unknown>,
    unsubscribe?: (data?: unknown) => Promise<unknown>,
] {
    const [thingLoading, thingError, thingJSON] = useFetch(('serviceWorker' in navigator && 'PushManager' in window) ? thingUrl : '');
    const thing = useJSON(thingJSON);
    const safeThing = zThing.safeParse(thing);
    const [eventError, eventAffordanceJSON] = useTDToEventAffordance(thingJSON, eventName);
    const eventDescription: unknown = useMemo(() =>
        eventAffordanceJSON.startsWith('{') ? JSON.parse(eventAffordanceJSON) : null,
    [eventAffordanceJSON]);
    const safeEventAffordance = zEventAffordance.safeParse(eventDescription);
    const eventFormSubscribe = useMemo(() => {
        return safeEventAffordance.data?.forms?.find(form => (
            hasOp(form.op, 'subscribeevent')
        ));
    }, [eventDescription]);
    const eventUrlSubscribe = !!thingUrl && !!eventFormSubscribe && new URL(eventFormSubscribe.href, safeThing.data?.base ?? thingUrl).toString();
    const vapidPublicKey = !!thingUrl && !!eventFormSubscribe && eventFormSubscribe.vapidPublicKey;
    const eventFormUnsubscribe = useMemo(() => {
        return safeEventAffordance.data?.forms?.find(form => (
            hasOp(form.op, 'unsubscribeevent')
        ));
    }, [eventDescription]);
    const eventUrlUnsubscribe = !!thingUrl && !!eventFormUnsubscribe && new URL(eventFormUnsubscribe.href, safeThing.data?.base ?? thingUrl).toString();

    const subscribe = useMemo(() => {
        if (!eventUrlSubscribe || !vapidPublicKey) {
            return;
        }
        return async (): Promise<unknown> => {     
            try {
                const subscription = await swRegistration?.pushManager.getSubscription();
                if (!subscription) {
                    const newSubscription = await swRegistration?.pushManager.subscribe({
                        userVisibleOnly: true,
                        applicationServerKey: vapidPublicKey,
                    });
                    if (newSubscription) {
                        const res = await fetch(eventUrlSubscribe, {
                            method: eventFormSubscribe['htv:methodName'] ?? 'post',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify(newSubscription),
                        });
                        if (!res.ok) {
                            throw Error(`HTTP ${res.status} (${res.statusText})`);
                        }
                        return await res.json();
                    }
                }
            } catch (error) {
                console.error('Error:', error);
            }
        }
    }, [eventFormSubscribe, eventUrlSubscribe, vapidPublicKey]);

    const unsubscribe = useMemo(() => {
        return async (): Promise<void> => {
            try {
                const subscription = await swRegistration?.pushManager.getSubscription();
                if (subscription) {
                    subscription.unsubscribe();
                }
            } catch (error) {
                console.error('Error:', error);
            }
        }
    }, [eventFormUnsubscribe, eventUrlUnsubscribe]);

    return [
        eventDescription,
        subscribe,
        unsubscribe,
    ];
}