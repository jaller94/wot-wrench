import React from 'react';

export function useTDToPropertyAffordance(json: string, propertyName: string): [string, string] {
    if (json === '' || !json.startsWith('{')) {
        return ['Not JSON', ''];
    }
    
    const data = JSON.parse(json) as unknown;

    if (typeof data !== 'object' || !data) {
        return ['Expected Thing Description', ''];
    }

    if (!('properties' in data) || !data.properties || typeof data.properties !== 'object') {
        return ['Missing object: properties', ''];
    }

    if (data.properties[propertyName] === undefined) {
        return ['Value for propertyName is missing', ''];
    }

    return ['', JSON.stringify(data.properties[propertyName])];
}

export function useTDToActionAffordance(json: string, actionName: string): [string, string] {
    if (json === '' || !json.startsWith('{')) {
        return ['Not JSON', ''];
    }
    
    const data = JSON.parse(json) as unknown;

    if (typeof data !== 'object' || !data) {
        return ['Expected Thing Description', ''];
    }

    if (!('actions' in data) || !data.actions || typeof data.actions !== 'object') {
        return ['Missing object: actions', ''];
    }

    if (!(actionName in data.actions) || !data.actions[actionName] || typeof data.actions[actionName] !== 'object') {
        return ['Value for actionName is missing', ''];
    }

    return ['', JSON.stringify(data.actions[actionName])];
}

export function useTDToEventAffordance(json: string, eventName: string): [string, string] {
    if (json === '' || !json.startsWith('{')) {
        return ['Not JSON', ''];
    }
    
    const data = JSON.parse(json) as unknown;

    if (typeof data !== 'object' || !data) {
        return ['Expected Thing Description', ''];
    }

    if (!('events' in data) || !data.events || typeof data.events !== 'object') {
        return ['Missing object: events', ''];
    }

    if (!(eventName in data.events) || !data.events[eventName] || typeof data.events[eventName] !== 'object') {
        return ['Value for eventsName is missing', ''];
    }

    return ['', JSON.stringify(data.events[eventName])];
}

