import React, { useEffect, useState } from 'react';

type CachedFetch = { ok: boolean, status: number, text: string };

class SDK {
    private cachedRequests = new Map<string, Promise<CachedFetch>>();

    async fetch(url: string): Promise<CachedFetch> {
        let cachedRequest = this.cachedRequests.get(url);
        if (!cachedRequest) {
            cachedRequest = this.fetchInner(url);
            this.cachedRequests.set(url, cachedRequest);
            // TODO: Test before uncommenting
            // this.limitCacheSize()
        }
        return cachedRequest;
    }

    async fetchInner(url: string): Promise<CachedFetch> {
        const res = await fetch(url);
        const result = {
            ok: res.ok,
            status: res.status,
            text: await res.text(),
        };
        return result;
    }

    limitCacheSize() {
        if (this.cachedRequests.size > 300) {
            this.cachedRequests.delete(this.cachedRequests.keys().next().value);
        }
    }
}

const sdk = new SDK();

export function useFetch(url: string): [boolean, string, string] {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<string>('');
    const [text, setText] = useState<string>('');

    useEffect(() => {
        if (!url) {
            return;
        }
        setLoading(true);
        setError('')
        setText('');
        const func = async function() {
            try {
                const res = await sdk.fetch(url);
                if (!res.ok) {
                    setError(`HTTP error ${res.status}`);
                    setText('');
                    return;
                }
                setError('');
                setText(res.text);
            } catch (err) {
                setError(err.message);
                console.error('useFetch error', err);
            } finally {
                setLoading(false);
            }
        };
        func();
    }, [url]);

    return [loading, error, text];
}