import React, { useMemo, useState } from 'react';

export function useJSON(json: string): unknown | undefined {
    return useMemo(() => {
        try {
            return JSON.parse(json);
        } catch {
            return undefined;
        }
    }, [json]);
}