import React, { useEffect, useMemo, useState } from 'react';
import { useTDToPropertyAffordance } from './useTDToAffordances';
import { useFetch } from './useFetch';

export function useWebsocketProp(thingUrl: string, thingJson: string, property: string): [boolean, string, string] {
    const [propError, propDescriptionJson] = useTDToPropertyAffordance(thingJson || '', property);
    const propDescription = propDescriptionJson.startsWith('{') ? JSON.parse(propDescriptionJson) : {};
    const wsLink = useMemo(() => {
        const data = thingJson.startsWith('{') ? JSON.parse(thingJson) : undefined;
        return data?.links?.find(link => link.rel === 'alternate' && /^wss?:\/\//.test(link.href));
    }, [thingJson]);
    const propertyLink = useMemo(() => {
        const data = thingJson.startsWith('{') ? JSON.parse(thingJson) : undefined;
        return data?.links?.find(link => link.rel === 'properties');
    }, [thingJson]);
    const propertyUrl = !!thingUrl && !!propertyLink && new URL(propertyLink.href, thingUrl).toString();
    const [propValueLoading, propValueError, propValueJson] = useFetch(propertyUrl || '');
    const propValue = useMemo(() => {
        return propValueJson.startsWith('{') ? JSON.parse(propValueJson) : {};
    }, [propValueJson]);
    const [value, setValue] = useState<unknown>(undefined);

    useEffect(() => {
        setValue(value => {
            return value ?? propValue[property];
        });
    }, [propValue[property]]);

    useEffect(() => {
        if (!wsLink) {
            return;
        }
        const webSocket = new WebSocket(wsLink.href);
        webSocket.addEventListener('open', event => {
            webSocket.send(JSON.stringify({
                messageType: 'addEventSubscription',
                data: {
                    [property]: true,
                },
            }));
            console.debug('ws open');
        });
        webSocket.addEventListener('message', (event) => {
            console.debug('ws message', event);
            const msg = JSON.parse(event.data);
            console.debug(msg);
            if (msg.messageType === 'propertyStatus' && msg.data[property] !== undefined) {
                setValue(msg.data[property]);
            }
        });
        webSocket.addEventListener('close', (event) => {
            console.debug('ws close', event);
        });
        webSocket.addEventListener('error', (event) => {
            console.error('ws error', event);
        });
        return () => {
            webSocket.close();
        };
    }, [wsLink, property]);

    return [false, '', JSON.stringify({[property]: value})];
}