import React, { useEffect, useState } from 'react';

export function useJSONFetch(url: string): [boolean, string, unknown] {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<string>('');
    const [data, setData] = useState<Record<string, unknown>>({});

    useEffect(() => {
        const func = async function() {
            setLoading(true);
            try {
                const res = await fetch(url);
                if (!res.ok) {
                    setError(`HTTP error ${res.status}`);
                    setData({});
                    return;
                }
                try {
                    const data = await res.json();
                    setError('');
                    setData(data);
                } catch (err) {
                    setError('Invalid JSON');
                    console.error(err);
                }
            } catch (err) {
                setError(err.message);
                console.error(err);
            } finally {
                setLoading(false);
            }
        };
        func();
    }, [url]);

    return [loading, error, data];
}