import React, { useEffect, useState } from 'react';

export function useRepeatedFetch(url: string, intervalDuration = 15000): [boolean, string, string] {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<string>('');
    const [text, setText] = useState<string>('');
    const [counter, setCounter] = useState<number>(0);
    const [gaveUp, setGaveUp] = useState<boolean>(false);

    useEffect(() => {
        setLoading(true);
        setCounter(0);
        setGaveUp(!url);
    }, [url]);

    useEffect(() => {
        if (!url) {
            setLoading(false);
            setError('No suitable URL');
            return;
        }
        const func = async function() {
            const res = await fetch(url);
            if (!res.ok) {
                setLoading(false);
                setError(`HTTP error ${res.status}`);
                if (res.status === 403 || res.status === 404) {
                    setGaveUp(true);
                }
                return;
            }
            const data = await res.text();
            setLoading(false);
            setError('');
            setText(data);
        };
        func();
    }, [url, counter]);

    useEffect(() => {
        if (gaveUp) {
            return;
        }
        const timeout = setInterval(() => {
            setCounter(count => count + 1);
        }, intervalDuration);
        return () => {
            clearInterval(timeout);
        }
    }, [gaveUp, intervalDuration]);

    return [loading, error, text];
}