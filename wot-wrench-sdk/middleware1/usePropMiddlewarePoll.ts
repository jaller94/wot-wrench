import React, { useEffect, useState } from 'react';
import z from 'zod';
import { useRepeatedFetch } from '../single/useRepeatedFetch';
import { usePropWrite } from './usePropWrite';
import { type ConnectionFunctionReturn } from './usePropMiddleware';

function hasOp(ops: unknown, op: string) {
    return Array.isArray(ops) ? ops.includes(op) : ops === op;
}

const zThing = z.object({
    base: z.string().optional(),
});

const zPropertyAffordance = z.object({
    forms: z.array(z.object({
        href: z.string(),
        op: z.union([z.string(), z.array(z.string())]),
    })).optional(),
});

export function useMiddlewarePoll(thingUrl: string, propertyName: string, thing: unknown, propDescription: unknown): ConnectionFunctionReturn {
    const safeThing = zThing.safeParse(thing);
    const safePropertyAffordance = zPropertyAffordance.safeParse(propDescription);
    const intervalDuration = 10000;
    const [propValue, setPropValue] = useState<unknown>();
    const propertyFormRead = safePropertyAffordance.data?.forms?.find(form => hasOp(form.op, 'readproperty'));
    const propertyUrlRead = !!thingUrl && !!propertyFormRead && new URL(propertyFormRead.href, safeThing.data?.base ?? thingUrl).toString();
    const [propValueLoading, propValueError, propValueJson] = useRepeatedFetch(propertyUrlRead || '', intervalDuration);
    const [setValue] = usePropWrite(thingUrl, propertyName, thing, propDescription);

    useEffect(() => {
        if (propValueJson === '') {
            return undefined;
        }
        setPropValue(JSON.parse(propValueJson));
    }, [propValueJson]);

    return {
        loading: propValueLoading,
        error: propValueError,
        value: propValue,
        setter: setValue,
        updateFrequency: intervalDuration,
        next: !!thingUrl && !!thing && !propertyUrlRead,
    };
}