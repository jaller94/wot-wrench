import React, { useEffect, useMemo, useState } from 'react';
import { useFetch } from '../single/useFetch';
import { useTDToPropertyAffordance } from '../single/useTDToAffordances';
import { useMiddlewareSSE } from './usePropMiddlewareSSE';
import { useMiddlewarePoll } from './usePropMiddlewarePoll';
import { useMiddlewareWebSocketAttempt2 } from './usePropMiddlewareWebSocketAttempt2';

export type ConnectionType = 'websocket' | 'sse' | 'repeated-polling' | 'none';

export type ConnectionFunctionReturn = {
    loading: boolean,
    error: string,
    value: unknown,
    setter?: (value: unknown) => Promise<void>,
    next: boolean,
    updateFrequency: number | undefined,
};

const noop = Promise.reject;

const CONNECTION_TYPE_ORDER: ConnectionType[] = ['websocket', 'repeated-polling', 'none'];

export const usePropMiddleware = (thingUrl: string, propertyName: string, repeated = true): [
    loading: boolean,
    error: string,
    propertyAffordance: Record<string, unknown> | undefined,
    value: unknown,
    setter: ((value: unknown) => Promise<void>) | undefined,
    updateFrequency: number | undefined,
    connectionType: ConnectionType,
] => {
    const [connectionType, setConnectionType] = useState<ConnectionType>(CONNECTION_TYPE_ORDER[0]!);
    const map = new Map<ConnectionType, ConnectionFunctionReturn>();
    const [thingLoading, thingError, thing] = useFetch(thingUrl);
    const [propError, propDescriptionJson] = useTDToPropertyAffordance(thing, propertyName);
    const propDescription = useMemo(() => {
        return propDescriptionJson.startsWith('{') ? JSON.parse(propDescriptionJson) : undefined;
    }, [propDescriptionJson]);
    map.set('websocket', useMiddlewareWebSocketAttempt2(connectionType === 'websocket' ? thingUrl : '', propertyName, thing, propDescription));
    // map.set('sse', useMiddlewareSSE(connectionType === 'sse' ? thingUrl : '', propertyName, thing, propDescription));
    map.set('repeated-polling', useMiddlewarePoll(connectionType === 'repeated-polling' ? thingUrl : '', propertyName, thing, propDescription));
    const result = map.get(connectionType);

    useEffect(() => {
        // Reset, when the thingUrl changes.
        if (connectionType !== CONNECTION_TYPE_ORDER[0]) {
            setConnectionType(CONNECTION_TYPE_ORDER[0]!);
        }
    }, [thingUrl]);

    useEffect(() => {
        // Advance to the next middleware, if the current one gave up.
        const index = CONNECTION_TYPE_ORDER.findIndex(v => v === connectionType);
        if (!!thing && !!result?.next) {
            const newConnectionType = CONNECTION_TYPE_ORDER[index + 1];
            if (newConnectionType) {
                setConnectionType(newConnectionType);
            }
        }
    }, [thingUrl, result?.next]);

    return [
        thingLoading || (result ? result.loading : false),
        thingError || propError || (result ? result.error : 'gave up'),
        propDescription,
        result?.value,
        result?.setter ?? noop,
        result?.updateFrequency,
        connectionType,
    ];
}