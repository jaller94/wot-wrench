import React, { useMemo } from 'react';
import z from 'zod';

const zThing = z.object({
    base: z.string().optional(),
});

const zPropertyAffordance = z.object({
    forms: z.array(z.object({
        href: z.string(),
        op: z.union([z.string(), z.array(z.string())]),
    })).optional(),
});

export function usePropWrite(thingUrl: string, propertyName: string, thing: unknown, propDescription: unknown): [setter?: (obj: unknown) => Promise<void>] {
    const safeThing = zThing.safeParse(thing);
    const safePropertyAffordance = zPropertyAffordance.safeParse(propDescription);
    const propertyFormWrite = safePropertyAffordance.data?.forms?.find(form => Array.isArray(form.op) ? form.op.includes('writeproperty') : form.op === 'writeproperty');
    const propertyUrlWrite = !!thingUrl && !!propertyFormWrite && new URL(propertyFormWrite.href, safeThing.data?.base ?? thingUrl).toString();

    const setValue = useMemo(() => {
        if (!propertyUrlWrite) {
            return;
        }
        return async (obj: unknown) => {
            const res = await fetch(propertyUrlWrite, {
                method: 'put',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(obj),
            });
            if (!res.ok) {
                throw Error(`HTTP Error ${res.status}: ${res.statusText}`);
            }
        }
    }, [propertyUrlWrite]);

    return [
        setValue,
    ];
}
