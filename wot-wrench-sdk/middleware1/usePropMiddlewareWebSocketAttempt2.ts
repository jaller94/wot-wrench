import React, { useEffect, useState } from 'react';
import z from 'zod';
import { usePropWrite } from './usePropWrite';
import { addPropertyListener, removePropertyListener } from '../sdk/attempt-2';
import type { ConnectionFunctionReturn } from './usePropMiddleware';

const hasOp = (ops: unknown, op: string): boolean => {
    return Array.isArray(ops) ? ops.includes(op) : ops === op;
};

const zThing = z.object({
    base: z.string().optional(),
});

const zPropertyAffordance = z.object({
    forms: z.array(z.object({
        href: z.string(),
        op: z.union([z.string(), z.array(z.string())]),
    })).optional(),
});

export function useMiddlewareWebSocketAttempt2(thingUrl: string, propertyName: string, thing: unknown, propDescription: unknown): ConnectionFunctionReturn {
    const safeThing = zThing.safeParse(thing);
    const safePropertyAffordance = zPropertyAffordance.safeParse(propDescription);
    const [propValueLoading, setPropValueLoading] = useState<boolean>(true);
    const [propValueError, setPropValueError] = useState<string>('');
    const [propValue, setPropValue] = useState<unknown>();
    const propertyFormObserveWebsocket = safePropertyAffordance.data?.forms?.find(form => hasOp(form.op, 'observeproperty') && /^wss?:\/\//.test(form.href));
    const propertyUrlObserveWebsocket = !!thingUrl && !!propertyFormObserveWebsocket && new URL(propertyFormObserveWebsocket.href, safeThing.data?.base ?? thingUrl).toString();
    const [setValue] = usePropWrite(thingUrl, propertyName, thing, propDescription);

    useEffect(() => {
        if (!propertyUrlObserveWebsocket) {
            setPropValueLoading(false);
            return;
        }
        setPropValueLoading(true);
        setPropValue(undefined);
        const func = (data: unknown) => {
            console.debug('event', data);
            setPropValue(data);
        };
        addPropertyListener(propertyUrlObserveWebsocket, thingUrl, propertyName, func);
        return () => {
            removePropertyListener(propertyUrlObserveWebsocket, thingUrl, propertyName, func);
        };
    }, [propertyName, propertyUrlObserveWebsocket, thingUrl]);

    return {
        loading: propValueLoading,
        error: propValueError,
        value: propValue,
        setter: setValue,
        next: !!thingUrl && !!thing && !propertyUrlObserveWebsocket,
        updateFrequency: undefined,
    };
}