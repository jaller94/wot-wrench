import React, { useEffect, useState } from 'react';
import z from 'zod';
import { usePropWrite } from './usePropWrite';
import { type ConnectionFunctionReturn } from './usePropMiddleware';

let inUse = 0;

function hasOp(ops: unknown, op: string) {
    return Array.isArray(ops) ? ops.includes(op) : ops === op;
}

const zThing = z.object({
    base: z.string().optional(),
});

const zPropertyAffordance = z.object({
    forms: z.array(z.object({
        href: z.string(),
        op: z.union([z.string(), z.array(z.string())]),
        subprotocol: z.string().optional(),
    })).optional(),
});

export function useMiddlewareSSE(thingUrl: string, propertyName: string, thing: unknown, propDescription: unknown): ConnectionFunctionReturn {
    const safeThing = zThing.safeParse(thing);
    const safePropertyAffordance = zPropertyAffordance.safeParse(propDescription);
    const [propValueLoading, setPropValueLoading] = useState<boolean>(true);
    const [propValueError, setPropValueError] = useState<string>('');
    const [propValue, setPropValue] = useState<unknown>();
    const propertyFormObserveSSE = safePropertyAffordance.data?.forms?.find(form => hasOp(form.op, 'observeproperty') && form.subprotocol === 'sse');
    const propertyUrlObserveSSE = !!thingUrl && !!propertyFormObserveSSE && new URL(propertyFormObserveSSE.href, safeThing.data?.base ?? thingUrl).toString();
    const [setValue] = usePropWrite(thingUrl, propertyName, thing, propDescription);

    useEffect(() => {
        if (!propertyUrlObserveSSE || inUse >= 4) {
            setPropValueLoading(false);
            return () => {};
        }
        setPropValueLoading(true);
        try {
            const evtSource = new EventSource(propertyUrlObserveSSE);
            inUse++;
            
            evtSource.addEventListener(propertyName, (e) => {
                setPropValue(JSON.parse(e.data));
            });
            setPropValueLoading(false);
            return () => {
                evtSource.close();
                inUse--;
            };
        } catch (err) {
            console.error(err);
            setPropValueError(err instanceof Error ? err.message : 'Unknown error');
        }
    }, [propertyName, propertyUrlObserveSSE]);

    return {
        loading: propValueLoading,
        error: propValueError,
        value: propValue,
        setter: setValue,
        next: !!thingUrl && !!thing && !propertyUrlObserveSSE || inUse >= 4,
        updateFrequency: !propValueLoading && !propValueError ? 0 : undefined,
    };
}