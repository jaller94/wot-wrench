# WoT Wrench

A web app to control Web of Things devices.

https://wot-wrench.chrpaul.de

## Build

The webapp with bundled JavaScript libraries will be put into the folder `out`.

```bash
bun install
bun run build
```
