self.addEventListener('push', function(event) {
    const options = {
        body: event.data.text(),
        // icon: 'icon.png',
        // badge: 'badge.png'
    };

    console.log('Event on the Push API', event.data);

    event.waitUntil(
        self.registration.showNotification('Push Notification', options)
    );
});
