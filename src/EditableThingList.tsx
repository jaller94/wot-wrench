import React, { useCallback, useEffect, useId, useMemo, useState, type ChangeEventHandler, type FC } from 'react';
import { Thing } from './Thing';
import { Box, Button, FormControl, InputLabel, OutlinedInput, TextField } from '@mui/material';
import { ThingList } from './ThingList';
import { uniqueId } from './helpers';

type Thing = {
    id: string,
    href: string,
};

type ThingConfigProps = {
    thing: Thing,
    onChange: (thing: Thing) => void,
};

const ThingConfig: FC<ThingConfigProps> = ({thing, onChange}) => {
    const inputId = useId();
    const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
        onChange({
            ...thing,
            href: event.target.value,
        });
    };
    
    return (
        <FormControl fullWidth sx={{ m: 1 }}>
            <InputLabel
                htmlFor={inputId}
            >Thing URL</InputLabel>
            <OutlinedInput
                defaultValue={thing.href}
                id={inputId}
                label="Thing URL"
                type="url"
                onChange={handleChange}
            />
        </FormControl>
    );
}

const useJSONFromLocalStorage = (key: string): unknown => {
    const result = useMemo(() => {
        const jsonValue = globalThis.localStorage.getItem(key);
        if (typeof jsonValue !== 'string') {
            return undefined;
        }
        try {
            return JSON.parse(jsonValue);
        } catch {
            return undefined;
        }
    }, []);
    return result;
}

export const EditableThingList: FC = () => {
    const savedThingUrls = useJSONFromLocalStorage('things');
    const [things, setThings] = useState<Thing[]>(
        Array.isArray(savedThingUrls) ?
        savedThingUrls.map(v => ({
            ...v,
            id: uniqueId(),
        })) : [],
    );

    useEffect(() => {
        const json = JSON.stringify(things);
        if (globalThis.localStorage.getItem('things') !== json) {
            globalThis.localStorage.setItem('things', JSON.stringify(things));
        }
    }, [things]);

    const handleAdd = useCallback(() => {
        setThings(things => ([
            ...things,
            {
                id: uniqueId(),
                href: '',
            },
        ]));
    }, []);
    const handleChange = useCallback((thing: Thing) => {
        setThings(things => {
            const index = things.findIndex(t => t.id === thing.id);
            if (index === -1) {
                return things;
            }
            return [
                ...things.slice(0, index),
                thing,
                ...things.slice(index + 1),
            ];
        });
    }, []);
    const removeThing = useCallback((thingId: string) => {
        setThings(things => things.filter(t => t.id !== thingId));
    }, []);

    return (
        <Box
            sx={{
                '& .MuiTextField-root': { m: 1, width: '25ch' },
            }}
        >
            {things.map((thing) => {
                return (
                    <Box key={thing.id}>
                        <ThingConfig thing={thing} onChange={handleChange} />
                        <Button onClick={() => removeThing(thing.id)}>Remove</Button>
                    </Box>
                );
            })}
            {<Button
                color="primary"
                onClick={handleAdd}
            >New thing</Button>}
            <ThingList things={things} />
        </Box>
    );
}
