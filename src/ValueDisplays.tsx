import React, { type FC } from 'react';
import MaptilerBBoxMap from './components/custom-properties/MaptilerBBoxMap';
import MaptilerMap from './components/custom-properties/MaptilerMap';
import { extractNullableSchema } from './Inputs';
import { dataSchema } from './schema';
import MoodJar from './components/custom-properties/MoodJar';

type DisplaySwitchProps = {
    schema: Record<string, unknown>,
    value: unknown,
};

const style = {
    maxWidth: '100%',
};

export const Base64AudioDisplay: FC<DisplaySwitchProps> = ({schema, value}) => {
    return <audio controls
        src={`data:${schema.contentMediaType};${schema.contentEncoding},${value}`}
        style={style}
    />;
};

export const Base64ImageDisplay: FC<DisplaySwitchProps> = ({schema, value}) => {
    return <img
        src={`data:${schema.contentMediaType};${schema.contentEncoding},${value}`}
        style={style}
    />;
};

export const Base64VideoDisplay: FC<DisplaySwitchProps> = ({schema, value}) => {
    return <video controls
        src={`data:${schema.contentMediaType};${schema.contentEncoding},${value}`}
        style={style}
    />;
};

export const MaptilerView: FC<DisplaySwitchProps> = ({schema, value}) => {
    if (typeof value !== 'object' || !value) {
        return;
    }
    return <MaptilerMap
        center={value.center}
        markers={value.markers}
        zoom={value.zoom}
    />;
};

export const MaptilerBBoxView: FC<DisplaySwitchProps> = ({schema, value}) => {
    if (typeof value !== 'object' || !value) {
        return;
    }
    return <MaptilerBBoxMap
        interactive={false}
        bbox={value.bbox}
        markers={value.markers}
    />;
};

const isOfType = (schema: {'@type'?: unknown}, type: string) => {
    if (typeof schema['@type'] === 'string') {
        return schema['@type'] === type;
    }
    if (Array.isArray(schema['@type'])) {
        return schema['@type'].includes(type);
    }
    return false;
}

export const DisplaySwitch: FC<DisplaySwitchProps> = (props) => {
    const schemaResult = dataSchema.safeParse(props.schema);
    if (!schemaResult.success) {
        return <>{JSON.stringify(props.value)}</>;
        // return <p>Unsupported schema</p>;
    }
    const result = extractNullableSchema(schemaResult.data);
    const schema = result.schema;
    if (schema.type === 'string') {
        if (isOfType(schema, 'ColorProperty')) {
            // return <ColorInput {...props} />;
        }
        if (schema.contentEncoding === 'base64' && ['audio/ogg', 'audio/oga', 'audio/opus', 'audio/mpeg'].includes(schema.contentMediaType ?? '')) {
            return <Base64AudioDisplay {...props} schema={schema} />;
        }
        if (schema.contentEncoding === 'base64' && ['image/gif', 'image/jpeg', 'image/png', 'image/webp'].includes(schema.contentMediaType ?? '')) {
            return <Base64ImageDisplay {...props} schema={schema} />;
        }
        if (schema.contentEncoding === 'base64' && ['video/mpeg', 'video/mp4', 'video/webm'].includes(schema.contentMediaType ?? '')) {
            return <Base64VideoDisplay {...props} schema={schema} />;
        }
    }
    if (schema.type === 'object') {
        if (isOfType(schema, 'Maptiler')) {
            return <MaptilerView {...props} />;
        }
        if (isOfType(schema, 'MaptilerBBox')) {
            return <MaptilerBBoxView {...props} />;
        }
    }
    if (schema.type === 'array') {
        if (isOfType(schema, 'de.chrpaul.mood-jar.2025-01')) {
            return <MoodJar {...props} />;
        }
    }
    return <>{JSON.stringify(props.value)}</>;
};
