import React, { useCallback, type FC } from 'react';
import { Button, Card, CardActions, CardContent, CardHeader, Typography } from '@mui/material';
import { useEvent } from 'wot-wrench-sdk/complete/useEvent';

type EventProps = {
    thingUrl: string,
    eventName: string,
};

export const WoTEvent: FC<EventProps> = ({thingUrl, eventName}) => {
    const [eventDescription, subscribe, unsubscribe] = useEvent(thingUrl, eventName);

    const handleSubscribe = useCallback(() => {
        subscribe?.();
    }, [subscribe]);

    const handleUnsubscribe = useCallback(() => {
        unsubscribe?.();
    }, [unsubscribe]);

    if (!eventDescription) {
        return (
            <Card>
                Loading event…
            </Card>
        );
    }

    return (
        <Card>
            <CardHeader
                title={<Typography variant="h6">{eventDescription.title ?? eventName}</Typography>}
            />
            <CardContent>
                {eventDescription.description && (
                    <Typography>{eventDescription.description}</Typography>
                )}
                <p>Support for events is very experimental.</p>
            </CardContent>
            <CardActions>
                {subscribe ? <Button onClick={handleSubscribe}>Subscribe</Button> : <p>Cannot subscribe</p>}
                {unsubscribe ? <Button onClick={handleUnsubscribe}>Unsubscribe from all</Button> : <p>Cannot unsubscribe</p>}
            </CardActions>
        </Card>
    );
}
