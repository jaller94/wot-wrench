import React, { type FC, useRef, useEffect } from 'react';
import * as maptilersdk from '@maptiler/sdk';
import { styled } from '@mui/system';
import stylesheet from "@maptiler/sdk/dist/maptiler-sdk.css";
import './map.css';

export const MapWrap = styled('div')({
    position: 'relative',
    width: '100%',
    height: '500px'
});

export const MapContainer = styled('div')({
    position: 'absolute',
    top: '0',
    bottom: '0',
    width: '100%',
    height: '500px'
});

type MaptilerMapProps = {
    markers?: {
        color?: string,
        latitude: number,
        longitude: number,
    }[],
    center?: {latitude: number, longitude: number},
    zoom?: number,
};

const MaptilerMap: FC<MaptilerMapProps> = (props) => {
    const mapContainer = useRef<HTMLDivElement>(null);
    const map = useRef<maptilersdk.Map | null>(null);
    const markers = useRef<maptilersdk.Marker[]>([]);
    maptilersdk.config.apiKey = 'thXUOFuqf6C0JH7u80Ei';

    useEffect(() => {
        if (map.current) return; // stops map from intializing more than once

        map.current = new maptilersdk.Map({
            container: mapContainer.current!,
            style: maptilersdk.MapStyle.STREETS,
            ...(typeof props.center === 'object' ? { center: [props.center.longitude, props.center.latitude] } : undefined),
            ...(typeof props.center === 'number' ? { zoom: props.zoom } : undefined),
        });
    }, []);

    useEffect(() => {
        if (!map.current) return;
        map.current.easeTo({
            center: typeof props.center === 'object' ? [props.center.longitude, props.center.latitude] : undefined,
            zoom: props.zoom,
        });
    }, [props.center, props.zoom]);

    useEffect(() => {
        if (!map.current) return;

        for (const marker of props.markers ?? []) {
            markers.current.push(new maptilersdk.Marker({color: marker.color ?? '#FF0000'})
                .setLngLat([marker.longitude, marker.latitude])
                .addTo(map.current)
            );
        }
        return () => {
            markers.current.forEach(marker => marker.remove());
            markers.current = [];
        };
    }, [props.markers]);

    return (
        <MapWrap className="map-wrap">
            <link rel="stylesheet" href={stylesheet} />
            <MapContainer ref={mapContainer} className="map"/>
        </MapWrap>
    );
};

export default MaptilerMap;
