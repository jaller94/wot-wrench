import React, { type FC, useRef, useEffect } from 'react';
import * as maptilersdk from '@maptiler/sdk';
import { styled } from '@mui/system';
import stylesheet from "@maptiler/sdk/dist/maptiler-sdk.css";
import './map.css';

export const MapWrap = styled('div')({
    position: 'relative',
    width: '100%',
    height: '500px'
});

export const MapContainer = styled('div')({
    position: 'absolute',
    top: '0',
    bottom: '0',
    width: '100%',
    height: '500px'
});

type MaptilerBBoxMapProps = {
    interactive?: boolean,
    markers?: {
        color?: string,
        latitude: number,
        longitude: number,
    }[],
    bbox: [[number, number], [number, number]],
};

const MaptilerBBoxMap: FC<MaptilerBBoxMapProps> = (props) => {
    const mapContainer = useRef<HTMLDivElement>(null);
    const map = useRef<maptilersdk.Map | null>(null);
    const markers = useRef<maptilersdk.Marker[]>([]);
    maptilersdk.config.apiKey = 'thXUOFuqf6C0JH7u80Ei';

    console.error('bbox', props.bbox)

    useEffect(() => {
        if (map.current) return; // stops map from intializing more than once

        map.current = new maptilersdk.Map({
            container: mapContainer.current!,
            // interactive: props.interactive,
            // navigationControl: false,
            fullscreenControl: 'top-left',
            // geolocateControl: false,
            style: maptilersdk.MapStyle.STREETS,
            bounds: props.bbox,
        });

        return () => {
            map.current?.remove();
            map.current = null;
        };
    }, []);

    useEffect(() => {
        if (!map.current) return;
        const newCameraTransform = map.current.cameraForBounds(props.bbox);
        console.log(newCameraTransform, props.bbox);
        map.current.easeTo({
            ...newCameraTransform,
        });
    }, [props.bbox]);

    useEffect(() => {
        if (!map.current) return;

        for (const marker of props.markers ?? []) {
            markers.current.push(new maptilersdk.Marker({color: marker.color ?? '#FF0000'})
                .setLngLat([marker.longitude, marker.latitude])
                .addTo(map.current)
            );
        }
        return () => {
            markers.current.forEach(marker => marker.remove());
            markers.current = [];
        };
    }, [props.markers]);

    return (
        <div>
            <MapWrap className="map-wrap">
                <link rel="stylesheet" href={stylesheet} />
                <MapContainer ref={mapContainer} className="map"/>
            </MapWrap>
            <button type="button" onClick={() => {
                console.log(map.current?.getBounds());
            }}>print bbox</button>
        </div>
    );
};

export default MaptilerBBoxMap;
