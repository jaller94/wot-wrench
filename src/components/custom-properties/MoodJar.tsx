import { styled } from '@mui/system';
import React, { type FC } from 'react';

const Jar = styled('div')({
    width: '300px',
    height: '300px',
    position: 'relative',
});

const Marble = styled('div')({
    width: '32px',
    height: '32px',
    position: 'absolute',
    borderRadius: '100%',
});

type MoodJarProps = {
    value: {
        id: string,
        color: string,
        note: string,
        tags: string[],
    }[] | undefined,
};

const MoodJar: FC<MoodJarProps> = (props) => {
    if (!Array.isArray(props.value)) {
        return <p>Mood Jar not ready</p>;
    }
    return (
        <>
            <Jar>
                {props.value.map(mood => (
                    <Marble key={mood.id} style={{backgroundColor: mood.color}}>
                    </Marble>
                ))}
            </Jar>
            <ol>
                {props.value.map(mood => (
                    <li key={mood.id} style={{backgroundColor: mood.color}}>
                        {mood.tags.join(', ')} {mood.note}
                    </li>
                ))}
            </ol>
        </>
    );
};

export default MoodJar;
