import React, { type FC, useMemo } from 'react';
import { Box, Card, CardContent, Stack, styled } from '@mui/material';
import { usePropMiddleware } from 'wot-wrench-sdk/middleware1/usePropMiddleware';

const Item = styled(Box)(({ theme }) => ({
    padding: theme.spacing(1),
    textAlign: 'center',
}));

export const MoodCollectionTime: FC<{date: string | null}> = ({date}) => {
    if (typeof date !== 'string') {
        return 'never';
    }
    const d = new Date(date);
    if (Number.isNaN(d)) {
        return 'invalid';
    }
    if (d.toLocaleDateString() === new Date().toLocaleDateString()) {
        return d.toLocaleTimeString();
    }
    return d.toLocaleString();
}

type MoodCollectionProps = {
    moods: {
        id: string,
        name: string,
        mood: string,
        updated: string | null,
    }[],
};

export const MoodCollectionVisual: FC<MoodCollectionProps> = ({moods}) => {
    return (
        <Card component="article">
            <CardContent>
                <Stack direction="row" justifyContent="space-evenly" spacing={2}>
                    {moods.map(mood => (
                        <Item key={mood.id}>
                            <div>{mood.name ?? '<no name>'}</div>
                            <div>{mood.mood ?? '<empty>'}</div>
                            <div><MoodCollectionTime date={mood.updated} /></div>
                        </Item>
                    ))}
                </Stack>
            </CardContent>
        </Card>
    );
}


type ThingProps = {
    thingUrl: string,
};

export const MoodCollection: FC<ThingProps> = ({thingUrl}) => {
    // const [loading, thingError, thing] = useThing(thingUrl);
    const moodUrls = usePropMiddleware(thingUrl, 'moods')[3];
    const mood0ThingUrl = Array.isArray(moodUrls) && moodUrls.length > 0 ? new URL(moodUrls[0], thingUrl).toString() : '';
    const mood1ThingUrl = Array.isArray(moodUrls) && moodUrls.length > 1 ? new URL(moodUrls[1], thingUrl).toString() : '';
    const mood0Mood = usePropMiddleware(mood0ThingUrl, 'mood')[3];
    const mood1Mood = usePropMiddleware(mood1ThingUrl, 'mood')[3];
    const mood0Name = usePropMiddleware(mood0ThingUrl, 'name')[3];
    const mood1Name = usePropMiddleware(mood1ThingUrl, 'name')[3];
    const mood0Modified = usePropMiddleware(mood0ThingUrl, 'modified')[3];
    const mood1Modified = usePropMiddleware(mood1ThingUrl, 'modified')[3];
    const moods = useMemo(() => {
        if (typeof mood0Mood !== 'string' || typeof mood1Mood !== 'string') {
            return [];
        }
        if (typeof mood0Name !== 'string' || typeof mood1Name !== 'string') {
            return [];
        }
        if ((typeof mood0Modified !== 'string' && mood0Modified !== null) || (typeof mood1Modified !== 'string' && mood1Modified !== null)) {
            return [];
        }
        return [
            {
                id: '0',
                name: mood0Name,
                mood: mood0Mood,
                updated: mood0Modified,
            },
            {
                id: '1',
                name: mood1Name,
                mood: mood1Mood,
                updated: mood1Modified,
            },
        ];
    }, [mood0Mood, mood0Name, mood0Modified, mood1Mood, mood1Name, mood1Modified]);
    // if (loading) {
    //     return <p>Loading thing…</p>;
    // }
    // if (thingError || !thing) {
    //     return <p>{thingError}</p>;
    // }
    return (
        <MoodCollectionVisual
            moods={moods}
        />
    );
}

