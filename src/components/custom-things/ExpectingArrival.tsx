import React, { type FC, useMemo } from 'react';
import { Box, Card, CardContent, Stack, styled } from '@mui/material';
import { usePropMiddleware } from 'wot-wrench-sdk/middleware1/usePropMiddleware';
import Maptiler from '../custom-properties/MaptilerMap';

const Item = styled(Box)(({ theme }) => ({
    padding: theme.spacing(1),
    textAlign: 'center',
}));

type ThingProps = {
    thingUrl: string,
};

export const MoodCollection: FC<ThingProps> = ({thingUrl}) => {
    const position = usePropMiddleware(thingUrl, 'position')[3];
    const destination = usePropMiddleware(thingUrl, 'destination')[3];
    const path = usePropMiddleware(thingUrl, 'path')[3];
    const name = usePropMiddleware(thingUrl, 'name')[3];
    const avatar = usePropMiddleware(thingUrl, 'avatar')[3];
    const eta = usePropMiddleware(thingUrl, 'eta')[3];
    const status = usePropMiddleware(thingUrl, 'status')[3];
    // if (loading) {
    //     return <p>Loading thing…</p>;
    // }
    // if (thingError || !thing) {
    //     return <p>{thingError}</p>;
    // }
    const markers = useMemo(() => {
        const markers: {
            color: string,
            latitude: number,
            longitude: number,
        }[] = [];
        if (typeof position === 'object' && position) {
            markers.push({
                color: '#0f0',
                ...position,
            });
        }
        if (typeof destination === 'object' && destination) {
            markers.push({
                color: '#f00',
                ...destination,
            });
        }
        return markers;
    }, [position, destination]);
    return (
        <Card component="article">
            <CardContent>
                {typeof name === 'string' && <p>Tracking {name}</p>}
                {typeof eta === 'string' && <p>Estimated arrival <strong>{eta}</strong></p>}
                {typeof status === 'string' && <p>{status}</p>}
                <Maptiler
                    markers={markers}
                />
            </CardContent>
        </Card>
    );
}

