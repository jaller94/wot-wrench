import React, {
    type ChangeEventHandler,
    type FC,
    type FormEventHandler,
    Fragment,
    type MouseEventHandler,
    useCallback,
    useState,
} from 'react';
import { Button, Card, CardActions, CardContent, CardHeader, TextareaAutosize, Typography } from '@mui/material';
import { useAction } from 'wot-wrench-sdk/complete/useAction';
import z from 'zod';
import { NullableInputSwitch } from './Inputs';

type ActionProps = {
    thingUrl: string,
    actionName: string,
};

const download = (filename: string, dataUri: string) => {
    const element = document.createElement('a');
    element.setAttribute('href', dataUri);
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

const BodyTextInput: FC<{
    actionDescription: Record<string, unknown>,
    body: string,
    onChange: (v: string) => void
}> = ({ actionDescription, body, onChange }) => {
    const handleChange: ChangeEventHandler<HTMLTextAreaElement> = useCallback((event) => {
        onChange(event.target.value);
    }, []);
    const handleInferClick: MouseEventHandler<HTMLButtonElement> = useCallback(() => {
        const a = new Map<string, unknown>();
        if (typeof actionDescription.input !== 'object' || !actionDescription.input || !('properties' in actionDescription.input) || typeof actionDescription.input.properties !== 'object' || !actionDescription.input.properties) {
            return;
        }
        for (const [key, schema] of Object.entries(actionDescription.input.properties)) {
            a.set(key, schema.type === 'integer' || schema.type === 'number' ? 0 : '');
        }
        onChange(JSON.stringify(Object.fromEntries(a.entries()), undefined, 2));
    }, [actionDescription]);
    return (<>
        <TextareaAutosize
            value={body}
            onChange={handleChange}
        />
        <Button
            type='button'
            onClick={handleInferClick}
        >Infer JSON</Button>
    </>);
};

const zDownloadResponse = z.object({
    content: z.string(),
    contentEncoding: z.string().optional(),
    contentType: z.string().optional(),
    contentMediaType: z.string().optional(),
    filename: z.string(),
});

const toDataUri = (data: z.infer<typeof zDownloadResponse>): string => {
    if (data.contentEncoding === 'base64') {
        return `data:${data.contentMediaType};base64,${data.content}`;
    }
    return `data:${data.contentType ?? 'plain/text'};charset=utf-8,${encodeURIComponent(data.content)}`;
}

export const Action: FC<ActionProps> = ({thingUrl, actionName}) => {
    const [actionDescription, invokeAction] = useAction(thingUrl, actionName);
    const [body, setBody] = useState('{}');

    const handleSubmit: FormEventHandler<HTMLFormElement> = useCallback(async (event) => {
        event.preventDefault();
        if (!invokeAction) {
            alert('No supported "invokeaction" operation found.');
            return;
        }
        try {
            const res = await invokeAction(JSON.parse(body));
            const safeDownloadRes = zDownloadResponse.safeParse(res);
            if (safeDownloadRes.success) {
                const { data } = safeDownloadRes;
                download(data.filename, toDataUri(data));
                return;
            }
            if (res !== undefined) {
                alert(JSON.stringify(res, undefined, 2));
            }
        } catch (err) {
            console.error('Action.handleSubmit error', err);
            alert(err);
        }
    }, [body, invokeAction]);

    const handleChange = (propertyName: string) => (value: unknown) => {
        setBody(body => {
            const data = JSON.parse(body);
            // TODO: This is a vulnerability because propertyName is user input
            data[propertyName] = value;
            return JSON.stringify(data, undefined, 2);
        });
    };

    if (!actionDescription) {
        return (
            <Card>
                Loading action…
            </Card>
        );
    }

    return (<form onSubmit={handleSubmit}>
        <Card>
            <CardHeader
                title={<Typography variant="h6">{actionDescription.title ?? actionName}</Typography>}
            />
            <CardContent>
                {actionDescription.description && (
                    <Typography>{actionDescription.description}</Typography>
                )}
                {invokeAction && actionDescription.input && (
                    <div>
                        {actionDescription.input?.type === 'object' ? (
                            Object.entries(actionDescription.input.properties).map(([propertyName, propertySchema]) => {
                                const required = actionDescription.input.required?.includes(propertyName);
                                return (<Fragment key={propertyName}>
                                    <Typography>
                                        {propertySchema.title ?? propertyName}{required && '*'}
                                    </Typography>
                                    <NullableInputSwitch
                                        schema={propertySchema}
                                        required={required}
                                        value={JSON.parse(body)[propertyName]}
                                        onChange={handleChange(propertyName)}
                                    />
                                </Fragment>);
                            })
                        ) : (
                            <BodyTextInput
                                actionDescription={actionDescription}
                                body={body}
                                onChange={setBody}
                            />
                        )}
                    </div>
                )}
            </CardContent>
            <CardActions>
                {invokeAction ? <Button type="submit">Invoke</Button> : <p>Cannot invoke action</p>}
            </CardActions>
        </Card>
    </form>);
}
