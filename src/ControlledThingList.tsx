import React, { type FC, useMemo } from 'react';
import { Box } from '@mui/material';
import { ThingList } from './ThingList';
import { usePropMiddleware } from 'wot-wrench-sdk/middleware1/usePropMiddleware';

let lastId = 0; 

export const ControlledThingList: FC<{url: string}> = ({ url }) => {
    const [loading, error, propertyDescription, thingUrls] = usePropMiddleware(url, 'urls');
    const things = useMemo(() => {
        if (!Array.isArray(thingUrls)) {
            return [];
        }
        return thingUrls.map((url, i) => ({
            id: i.toString(),
            href: url,
        }));
    }, [thingUrls]);
    return (
        <Box
            sx={{
                '& .MuiTextField-root': { m: 1, width: '25ch' },
            }}
        >
            <ThingList things={things} />
        </Box>
    );
}
