import React, { type FC, useCallback, useEffect, useState } from 'react';
import { type ConnectionType, usePropMiddleware } from 'wot-wrench-sdk/middleware1/usePropMiddleware';
import { Typography } from '@mui/material';
import { NullableInputSwitch } from './Inputs';
import { DisplaySwitch } from './ValueDisplays';

type PropertyProps = {
    thingUrl: string,
    propertyName: string,
};

export const Property: FC<PropertyProps> = ({thingUrl, propertyName}) => {
    const [loading, error, propertyDescription, value, setValue] = usePropMiddleware(thingUrl, propertyName);
    const handleChange = useCallback((value: unknown) => {
        if (!setValue) {
            alert('Not ready to write to this property. Try again in a few seconds.');
            return;
        }
        setValue(value);
    }, [setValue]);
    if (loading) {
        return <p>Loading…</p>;
    }
    if (error) {
        return <p>{error}</p>;
    }

    return (
        <div>
            <label>
                <Typography variant="h6">
                    {propertyDescription.title ?? propertyName}
                </Typography>
                {propertyDescription.description && (
                    <Typography>{propertyDescription.description}</Typography>
                )}
                {propertyDescription.readOnly !== true && (
                    <NullableInputSwitch
                        schema={propertyDescription}
                        value={value}
                        onChange={handleChange}
                    />
                )}
                <span>{JSON.stringify(value)}</span> 
            </label>
        </div>
    );
};

const connectionTypeIndicators = new Map<ConnectionType, string>([
    ['websocket', '🔄'],
    ['sse', '📩'],
    ['repeated-polling', '↩️'],
    ['none', '📵'],
]);

export const ConnectionTypeIndicator: FC<{connectionType: ConnectionType}> = ({connectionType}) => {
    return connectionTypeIndicators.get(connectionType) ?? '?';
};

export const LiveEditProperty: FC<PropertyProps> = ({thingUrl, propertyName}) => {
    const [_loading, error, propertyDescription, value, setValue, _updateFrequency, connectionType] = usePropMiddleware(thingUrl, propertyName);
    const [editingValue, setEditingValue] = useState<unknown>();
    const [validValue, setValidValue] = useState<unknown>();
    const [focused, setFocused] = useState(false);
    const [valid, setValid] = useState(false);

    const handleChange = useCallback((editingValue: unknown, validValue: unknown) => {
        setEditingValue(editingValue);
        setValidValue(validValue);
        setValid(validValue !== undefined);
    }, []);

    const handleFocus = useCallback(() => setFocused(true), []);
    const handleBlur = useCallback(() => setFocused(false), []);

    useEffect(() => {
        if (!focused && validValue !== undefined) {
            setEditingValue(undefined);
            setValidValue(undefined);
        }
    }, [value]);

    useEffect(() => {
        if (validValue === undefined) {
            return;
        }
        if (!setValue) {
            alert('Cannot save because writeproperty is not ready');
            return;
        }
        if (focused) {
            const timeout = setTimeout(() => {
                setValue(validValue);
            }, 3000);
            return () => clearTimeout(timeout);
        } else {   
            setValue(validValue);
            setEditingValue(undefined);
            setValidValue(undefined);
        }
    }, [focused, validValue]);

    if (!propertyDescription) {
        return <p>Loading…</p>;
    }

    return (
        <div>
            <Typography variant="h6">
                {propertyDescription.title ?? propertyName}
                <ConnectionTypeIndicator connectionType={connectionType} />
            </Typography>
            {propertyDescription.description && (
                <Typography>{propertyDescription.description}</Typography>
            )}
            {error && <p>{error}</p>}
            {propertyDescription.readOnly !== true && (
                <NullableInputSwitch
                    schema={propertyDescription}
                    value={editingValue ?? validValue ?? value}
                    onChange={handleChange}
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                />
            )}
            {focused && (valid ? 'yes' : 'err')}
            {propertyDescription.writeOnly !== true && (
                <DisplaySwitch
                    schema={propertyDescription}
                    value={validValue ?? value}
                />
            )}
        </div>
    );
};
