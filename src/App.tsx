import React, { useEffect, useMemo, useState, type FC } from 'react';
import { CssBaseline, Stack } from "@mui/material";
import { AppBar, Container, Toolbar, Typography } from '@mui/material';
import { EditableThingList } from './EditableThingList';
import { ControlledThingList } from './ControlledThingList';
import { useAction } from 'wot-wrench-sdk/complete/useAction';
import { ThingList } from './ThingList';
import { uniqueId } from './helpers';

const PickerProcessor: FC = () => {
    const [picker, setPicker] = useState<string|null>(null);
    const [actionDescription, invokeAction] = useAction(picker ?? '', '');

    useEffect(() => {
        const handleHashChange = () => {
            const parsedHash = new URLSearchParams(globalThis.location.hash.slice(1));
            const pickerUrl = parsedHash.get('picker');
            setPicker(pickerUrl);
        };

        handleHashChange();
        globalThis.addEventListener('hashchange', handleHashChange);
        return () => {
            globalThis.removeEventListener('hashchange', handleHashChange);
        };
    }, []);

    useEffect(() => {
        if (!invokeAction) {
            return;
        }
        let stillValid = true;
        invokeAction().then((res: unknown) => {
            if (!stillValid) {
                return;
            }
            console.log(res);
        });
        return () => {
            stillValid = false;
        };
    }, [invokeAction]);

    return null;
};

export const App: FC = () => {
    const [controller, setController] = useState<string|null>(null);
    const [kioskMode, setKioskMode] = useState<boolean>(false);
    const [thingUrls, setThingUrls] = useState<string[]|undefined>(undefined);

    useEffect(() => {
        const handleHashChange = () => {
            const parsedHash = new URLSearchParams(globalThis.location.hash.slice(1));
            setController(parsedHash.get('controller'));
            const tryParam = parsedHash.get('try');
            console.log('tryParam', tryParam);
            if (tryParam) {
                setKioskMode(true);
                setThingUrls(tryParam.split(','));
            } else {
                setKioskMode(false);
                setThingUrls(undefined);
            }
        };

        handleHashChange();
        globalThis.addEventListener('hashchange', handleHashChange);
        return () => {
            globalThis.removeEventListener('hashchange', handleHashChange);
        };
    }, []);

    const things = useMemo(() => {
        if (thingUrls === undefined) {
            return;
        }
        return thingUrls.map(v => ({
            href: v,
            id: uniqueId(),
        }));
    }, [thingUrls]);

    return (<>
        <CssBaseline />
        <AppBar
            elevation={0}
            position="static"
        >
            <Container maxWidth="sm">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        WoT Wrench
                    </Typography>
                </Toolbar>
            </Container>
        </AppBar>
        <Container maxWidth="sm">
            <Stack spacing={2}>
                {controller ? <ControlledThingList url={controller} /> : (
                    things !== undefined ? <ThingList things={things} /> : <EditableThingList />
                )}
            </Stack>
        </Container>
        <PickerProcessor />
    </>);
}
