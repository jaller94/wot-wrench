import React, {
    type ChangeEventHandler,
    type FC,
    type MouseEventHandler,
    useCallback,
    useMemo,
    useState,
} from 'react';

import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { Textarea } from '@mui/joy';
import { Button, Checkbox, MenuItem, Select, type SelectChangeEvent, Slider, TextField } from '@mui/material';
import { styled } from '@mui/material';
import { dataSchema, type DataSchema } from './schema';

type InputSwitchProps = {
    schema: unknown,
    value: unknown,
    onChange: (editingValue: unknown, validValue: unknown) => void,
    onFocus: () => void,
    onBlur: () => void,
};

const VisuallyHiddenInput = styled('input')({
    clip: 'rect(0 0 0 0)',
    clipPath: 'inset(50%)',
    height: 1,
    overflow: 'hidden',
    position: 'absolute',
    bottom: 0,
    left: 0,
    whiteSpace: 'nowrap',
    width: 1,
});  

export const Base64Input: FC<InputSwitchProps & {accept?: string, label?: string}> = ({accept, label, onChange}) => {
    const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback((event) => {
        const file = event.target.files?.[0];
        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                const value = reader.result as string;
                onChange(value, value);
            };
            reader.onerror = (error) => {
                console.error('Error:', error);
            };
        }
    }, [onChange]);
    return <Button
        component="label"
        role={undefined}
        variant="contained"
        tabIndex={-1}
        startIcon={<CloudUploadIcon />}
    >
        {label ?? 'Upload file'}
        <VisuallyHiddenInput
            accept={accept}
            type="file"
            onChange={handleChange}
        />
    </Button>;
};

export const Base64AudioInput: FC<InputSwitchProps> = (props) => {
    return <Base64Input {...props} accept="audio/*" label="Upload audio" />;
};

export const Base64ImageInput: FC<InputSwitchProps> = (props) => {
    return <Base64Input {...props} accept="image/*" label="Upload image" />;
};

export const Base64VideoInput: FC<InputSwitchProps> = (props) => {
    return <Base64Input {...props} accept="video/*" label="Upload video" />;
};

export const StringEnumInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange = useCallback((event: SelectChangeEvent) => {
        const { value } = event.target;
        onChange(value, value);
    }, [onChange]);
    return <Select
        {...inputProps}
        value={value ?? ''}
        onChange={handleChange}
    >
        {schema.enum.map(value => (
            <MenuItem key={value} value={value}>{value}</MenuItem>
        ))}
    </Select>;
};

export const KahootInput: FC<InputSwitchProps> = ({value, onChange}) => {
    const handleClick: MouseEventHandler<HTMLButtonElement> = useCallback((event) => {
        const { value } = event.target.dataset;
        onChange(value, value);
    }, [onChange]);
    if (value) {
        return <div style={{display: 'grid', gap: '1em', gridTemplateColumns: '1fr 1fr'}}>
            {value === '1' && <Button size="large" variant="contained" sx={{background: 'red'}} disabled>A</Button>}
            {value === '2' && <Button size="large" variant="contained" sx={{background: 'blue'}} disabled>B</Button>}
            {value === '3' && <Button size="large" variant="contained" sx={{background: '#F6BE00'}} disabled>C</Button>}
            {value === '4' && <Button size="large" variant="contained" sx={{background: 'green'}} disabled>D</Button>}
        </div>;
    }
    return <div style={{display: 'grid', gap: '1em', gridTemplateColumns: '1fr 1fr'}}>
        <Button size="large" variant="contained" sx={{background: 'red'}} data-value="1" onClick={handleClick}>A</Button>
        <Button size="large" variant="contained" sx={{background: 'blue'}} data-value="2" onClick={handleClick}>B</Button>
        <Button size="large" variant="contained" sx={{background: '#F6BE00'}} data-value="3" onClick={handleClick}>C</Button>
        <Button size="large" variant="contained" sx={{background: 'green'}} data-value="4" onClick={handleClick}>D</Button>
    </div>;
};

export const ColorInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: React.ChangeEventHandler<HTMLInputElement> = useCallback((event) => {
        const { value } = event.target;
        onChange(value, value);
    }, [onChange]);
    return <input
        {...inputProps}
        disabled={schema.readOnly === true}
        type="color"
        value={value ?? ''}
        label={schema.title}
        onChange={handleChange}
    />;
};

export const TextInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback((event) => {
        const { value } = event.target;
        onChange(value, value);
    }, [onChange]);
    return <TextField
        {...inputProps}
        disabled={schema.readOnly}
        label={schema.title}
        value={value ?? ''}
        onChange={handleChange}
    />;
};

export const MultiLineTextInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: ChangeEventHandler<HTMLTextAreaElement> = useCallback((event) => {
        const { value } = event.target;
        onChange(value, value);
    }, [onChange]);
    return <Textarea
        style={{
            resize: 'vertical',
            width: '100%',
        }}
        {...inputProps}
        disabled={schema.readOnly}
        label={schema.title}
        value={value ?? ''}
        onChange={handleChange}
    />;
};

export const StringArrayInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: ChangeEventHandler<HTMLTextAreaElement> = useCallback((event) => {
        const { value } = event.target;
        const validValue = value.split('\n').map(v => v.trim()).filter(v => v !== '');
        onChange(value, validValue);
    }, [onChange]);
    return <Textarea
        style={{
            resize: 'vertical',
            width: '100%',
        }}
        {...inputProps}
        disabled={schema.readOnly}
        label={schema.title}
        value={Array.isArray(value) ? value.join('\n') : (value ?? '')}
        onChange={handleChange}
    />;
};

export const ObjectJSONInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: ChangeEventHandler<HTMLTextAreaElement> = useCallback((event) => {
        const { value } = event.target;
        let data;
        try {
            data = JSON.parse(value);
        } catch (err) {
            console.warn('ObjectJSONInput.handleChange JSON.parse error', err);
        }
        onChange(value, data);
    }, [onChange]);
    return <Textarea
        style={{
            resize: 'vertical',
            width: '100%',
        }}
        {...inputProps}
        disabled={schema.readOnly}
        label={schema.title}
        value={typeof value === 'object' ? JSON.stringify(value, undefined, 2) : (value ?? '')}
        onChange={handleChange}
    />;
};

export const NumberInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement> = useCallback((event) => {
        const parseFunc = schema.type === 'integer' ? Number.parseInt : Number.parseFloat;
        const newValue = parseFunc(event.target.value);
        onChange(event.target.value, !Number.isNaN(newValue) && Number.isFinite(newValue) ? newValue : undefined);
    }, [schema.type, onChange]);
    return <TextField
        {...inputProps}
        disabled={!!schema.readOnly}
        label={typeof schema.title === 'string' ? schema.title : undefined}
        min={typeof schema.minimum === 'number' ? schema.minimum : undefined}
        max={typeof schema.maximum === 'number' ? schema.maximum : undefined}
        step={schema.type === 'integer' ? 1 : (typeof schema.step === 'number' ? schema.step : undefined)}
        type="number"
        value={value ?? ''}
        onChange={handleChange}
    />;
};

export const NumberSliderInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement> = useCallback((event) => {
        const parseFunc = schema.type === 'integer' ? Number.parseInt : Number.parseFloat;
        const newValue = parseFunc(event.target.value);
        if (Number.isNaN(newValue)) {
            return;
        }
        onChange(event.target.value, Number.isFinite(newValue) ? newValue : undefined);
    }, [schema.type, onChange]);
    return <Slider
        {...inputProps}
        disabled={!!schema.readOnly}
        label={typeof schema.title === 'string' ? schema.title : undefined}
        min={typeof schema.minimum === 'number' ? schema.minimum : undefined}
        max={typeof schema.maximum === 'number' ? schema.maximum : undefined}
        step={schema.type === 'integer' ? 1 : (typeof schema.step === 'number' ? schema.step : undefined)}
        type="range"
        value={typeof value === 'number' ? value : 0}
        valueLabelDisplay="auto"
        onChange={handleChange}
    />;
};

export const BooleanInput: FC<InputSwitchProps> = ({schema, value, onChange, ...inputProps}) => {
    const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback((event) => {
        const { checked } = event.target;
        onChange(checked, checked);
    }, [onChange]);
    return <Checkbox
        {...inputProps}
        disabled={schema.readOnly}
        type="checkbox"
        checked={typeof value === 'boolean' ? value : false}
        onChange={handleChange}
    />;
};

const isOfType = (schema: {'@type'?: unknown}, type: string) => {
    if (typeof schema['@type'] === 'string') {
        return schema['@type'] === type;
    }
    if (Array.isArray(schema['@type'])) {
        return schema['@type'].includes(type);
    }
    return false;
}

/**
 * Basic detection for a schema that is either null or another type.
 * It will return the other type for the automatic input selection.
 */
export function extractNullableSchema(schema: DataSchema): {nullable: boolean, schema: PropertyAffordance} {
    const schemaResult = dataSchema.safeParse(schema);
    if (!schemaResult.success) {
        console.error(schema, schemaResult);
        throw Error('Invalid schema');
    }
    const validatedSchema = schemaResult.data;
    if (validatedSchema.oneOf?.length === 2) {
        const otherSchema = validatedSchema.oneOf.find(v => !('type' in v && v.type === 'null'));
        if (otherSchema) {
            return {
                nullable: true,
                schema: {
                    title: schema.title,
                    titles: schema.titles,
                    description: schema.description,
                    descriptions: schema.descriptions,
                    readOnly: schema.readOnly,
                    writeOnly: schema.writeOnly,
                    ...otherSchema,
                },
            }
        }
    }
    return {
        nullable: false,
        schema,
    }
}

export const InputSwitch: FC<InputSwitchProps> = (props) => {
    const schemaResult = dataSchema.safeParse(props.schema);
    if (!schemaResult.success) {
        console.error('schemaError', schemaResult.error);
        return <p>Unsupported schema</p>;
    }
    const schema = schemaResult.data;
    if (schema.type === 'string') {
        if (isOfType(schema, 'ColorProperty')) {
            return <ColorInput {...props} />;
        }
        if (isOfType(schema, 'Kahoot')) {
            return <KahootInput {...props} />;
        }
        if (isOfType(schema, 'MultiLineText')) {
            return <MultiLineTextInput {...props} />;
        }
        if (schema.contentEncoding === 'base64') {
            if (schema.contentMediaType?.startsWith('audio/')) {
                return <Base64AudioInput {...props} />;
            }
            if (schema.contentMediaType?.startsWith('image/')) {
                return <Base64ImageInput {...props} />;
            }
            if (schema.contentMediaType?.startsWith('video/')) {
                return <Base64VideoInput {...props} />;
            }
            return <Base64Input {...props} />;
        }
        if (schema.enum !== undefined) {
            return <StringEnumInput {...props} />;
        }
        return <TextInput {...props} />;
    }
    if (schema.type === 'integer' || schema.type === 'number') {
        if (typeof schema.minimum === 'number' && typeof schema.maximum === 'number' && schema.minimum === 0 && schema.maximum === 255) {
            return <NumberSliderInput {...props} />
        }
        return <NumberInput {...props} />;
    }
    if (schema.type === 'boolean') {
        return <BooleanInput {...props} />;
    }
    if (schema.type === 'array') {
        if (schema.items && schema.items.type === 'string') {
            return <StringArrayInput {...props} />;
        }
        return <ObjectJSONInput {...props} />;
    }
    if (schema.type === 'object') {
        return <ObjectJSONInput {...props} />;
    }
    return <p>No suitable input</p>;
};

export const NullableInputSwitch: FC<InputSwitchProps> = (props) => {
    const [editingValue, setEditingValue] = useState(props.value);
    const [validValue, setValidValue] = useState<unknown>(undefined);
    const [enabled, setEnabled] = useState(true);
    const result = useMemo(() => {
        const schemaResult = dataSchema.safeParse(props.schema);
        if (!schemaResult.success) {
            console.error('schemaError', schemaResult.error);
            return;
        }
        return extractNullableSchema(schemaResult.data);
    }, [props.schema]);
    if (!result) {
        return <p>Unsupported schema</p>;
    }
    const modifiedOnChange = useCallback((editingValue: unknown, validValue: unknown) => {
        setEditingValue(editingValue);
        setValidValue(validValue);
        props.onChange(editingValue, enabled ? validValue : null);
    }, [enabled]);
    const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback((event) => {
        setEnabled(event.target.checked);
        props.onChange(editingValue, event.target.checked ? validValue : null);
    }, [editingValue, validValue]);
    const input = <InputSwitch
        {...props}
        schema={result.schema}
        onChange={modifiedOnChange}
    />;
    if (result.nullable) {
        return <>
            <input type="checkbox" checked={enabled} onChange={handleChange} />
            {input}
        </>;
    }
    return input;
};
