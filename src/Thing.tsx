import React, { type FC, useMemo } from 'react';
import { Action } from './Action';
import { LiveEditProperty } from './Property';
import { useThing } from 'wot-wrench-sdk/complete/useThing';
import { Card, CardContent, Typography } from '@mui/material';
import ErrorBoundary from './ErrorBoundary';
import MaptilerMap from './components/custom-properties/MaptilerMap';
import { MoodCollection } from './components/custom-things/MoodCollection';
import { WoTEvent } from './Event';

type ThingProps = {
    thingUrl: string,
};

export const Thing: FC<ThingProps> = ({thingUrl}) => {
    const [loading, thingError, thing] = useThing(thingUrl);
    const markers = useMemo(() => {
        if (!thing) return;
        if (typeof thing['geo:lat'] === 'string' && typeof thing['geo:long'] === 'string') {
            const latitude = Number.parseFloat(thing['geo:lat']);
            const longitude = Number.parseFloat(thing['geo:long']);
            if (Number.isNaN(latitude) || Number.isNaN(longitude)) {
                return;
            }
            return [{
                latitude,
                longitude,
            }];
        }
    }, [thing]);
    const propertiesEntries = useMemo(() => {
        if (typeof thing?.properties !== 'object') {
            return [];
        }
        return [...Object.entries(thing.properties)];
    }, [thing?.properties]);
    const actionsEntries = useMemo(() => {
        if (typeof thing?.actions !== 'object') {
            return [];
        }
        return [...Object.entries(thing.actions)];
    }, [thing?.actions]);
    const eventsEntries = useMemo(() => {
        if (typeof thing?.events !== 'object') {
            return [];
        }
        return [...Object.entries(thing.events)];
    }, [thing?.events]);
    if (loading) {
        return <p>Loading thing…</p>;
    }
    if (thingError || !thing) {
        return <p>{thingError}</p>;
    }
    if (thing['@type'] === 'cp:MoodCollection') {
        return <MoodCollection
            thingUrl={thingUrl}
        />;
    }
    if (!thing) {
        return (
            <Card component="article">
                <CardContent>
                    Loading…
                </CardContent>
            </Card>
        );
    }
    return (
        <Card component="article">
            <CardContent>
                <Typography variant="h4">{thing.title}</Typography>
                {thing.description && (
                    <Typography>{thing.description}</Typography>
                )}
                {propertiesEntries.length !== 0 && <Typography variant="h5">Properties</Typography>}
                {propertiesEntries.map(([key, description]) => {
                    return <ErrorBoundary
                        key={key}
                        errorMessage={`Error while rendering property "${key}"`}
                    >
                        <LiveEditProperty
                            key={key}
                            thingUrl={thingUrl}
                            propertyName={key}
                        />
                    </ErrorBoundary>;
                })}
                {actionsEntries.length !== 0 && <Typography variant="h5">Actions</Typography>}
                {actionsEntries.map(([key, description]) => {
                    return <ErrorBoundary
                        key={key}
                        errorMessage={`Error while rendering action "${key}"`}
                    >
                        <Action
                            thingUrl={thingUrl}
                            actionName={key}
                        />
                    </ErrorBoundary>;
                })}
                {eventsEntries.length !== 0 && <Typography variant="h5">Events</Typography>}
                {eventsEntries.map(([key, description]) => {
                    return <ErrorBoundary
                        key={key}
                        errorMessage={`Error while rendering event "${key}"`}
                    >
                        <WoTEvent
                            thingUrl={thingUrl}
                            eventName={key}
                        />
                    </ErrorBoundary>;
                })}
                {markers && (<ErrorBoundary
                    errorMessage="Error while rendering a map"
                >
                    <MaptilerMap center={markers[0]} zoom={8} markers={markers} />
                </ErrorBoundary>)}
            </CardContent>
        </Card>
    );
}
