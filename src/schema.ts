import z from 'zod';

const basedataSchemaCommon = z.object({
    '@type': z.union([z.string(), z.array(z.string())]).optional(),
    title: z.string().optional(),
    titles: z.record(z.string(), z.string()).optional(),
    description: z.string().optional(),
    descriptions: z.record(z.string(), z.string()).optional(),
    readOnly: z.boolean().optional(),
    writeOnly: z.boolean().optional(),
});

type DataSchemaCommon = z.infer<typeof basedataSchemaCommon> & {
    allOf?: DataSchema[];
    anyOf?: DataSchema[];
    oneOf?: DataSchema[];
    not?: DataSchema;
};
  
const dataSchemaCommon: z.ZodType<DataSchemaCommon> = basedataSchemaCommon.extend({
    allOf: z.array(z.lazy(() => dataSchema)).optional(),
    anyOf: z.array(z.lazy(() => dataSchema)).optional(),
    oneOf: z.array(z.lazy(() => dataSchema)).optional(),
    not: z.lazy(() => dataSchema).optional(),
});

const typeSpecificDataSchema = z.discriminatedUnion('type', [
    z.object({
        type: z.literal('array'),
        items: z.object({
            type: z.string(),
        }),
    }),
    z.object({
        type: z.literal('boolean'),
    }),
    z.object({
        type: z.literal('integer'),
        minimum: z.number().optional(),
        maximum: z.number().optional(),
    }),
    z.object({
        type: z.literal('null'),
    }),
    z.object({
        type: z.literal('number'),
        minimum: z.number().optional(),
        maximum: z.number().optional(),
    }),
    z.object({
        type: z.literal('object'),
    }),
    z.object({
        type: z.literal('string'),
        enum: z.array(z.string()).optional(),
        contentMediaType: z.string().optional(),
        contentEncoding: z.string().optional(),
    }),
    z.object({
        type: z.undefined(),
    }),
]);

export const dataSchema = typeSpecificDataSchema.and(dataSchemaCommon);
export type DataSchema = z.infer<typeof dataSchema>;
