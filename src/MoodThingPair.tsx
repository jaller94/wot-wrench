import React, { type FC } from 'react';
import { LiveEditProperty } from './Property';
import { useThing } from 'wot-wrench-sdk/complete/useThing';
import { Card, CardContent, Typography } from '@mui/material';
import ErrorBoundary from './ErrorBoundary';
type ThingProps = {
    thingUrlA: string,
    thingUrlB: string,
};

export const Thing: FC<ThingProps> = ({thingUrlA, thingUrlB}) => {
    const [loadingThingA, errorThingA, thingA] = useThing(thingUrlA);
    const [loadingThingB, errorThingB, thingB] = useThing(thingUrlB);
    if (loadingThingA || loadingThingB) {
        return <p>Loading thing…</p>;
    }
    if (errorThingA || errorThingB || !thingA || !thingB) {
        return <p>{thingError}</p>;
    }
    return (
        <Card component="article">
            <CardContent>
                <Typography variant="h4">{thing.title}</Typography>
                {thing.description && (
                    <Typography>{thing.description}</Typography>
                )}
                {propertiesEntries.length !== 0 && <Typography variant="h5">Properties</Typography>}
                {propertiesEntries.map(([key, description]) => {
                    return <ErrorBoundary
                        key={key}
                        errorMessage={`Error while rendering property "${key}"`}
                    >
                        <LiveEditProperty
                            key={key}
                            thingUrl={thingUrl}
                            propertyName={key}
                        />
                    </ErrorBoundary>;
                })}
            </CardContent>
        </Card>
    );
}
