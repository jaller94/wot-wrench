import React, { type FC } from 'react';
import { Thing } from './Thing';
import { Stack } from '@mui/material';

export const ThingList: FC<{things: {id: string, href: string}[]}> = ({things}) => {
    return (
        <Stack spacing={2}>
            {things.map((thing) => {
                return (
                    <Thing key={thing.id} thingUrl={thing.href} />
                );
            })}
        </Stack>
    );
}
