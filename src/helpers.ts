let lastId = 0; 
export const uniqueId = function() {
    return `id-${lastId++}`;
};
