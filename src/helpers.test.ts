import {
    describe,
    expect,
    test,
} from 'bun:test';
import {
    uniqueId,
} from '../src/helpers.ts';

describe('uniqueId', () => {
    test('two calls yield different results', () => {
        expect(uniqueId()).not.toBe(uniqueId());
    });
    test('returns a string', () => {
        expect(typeof uniqueId()).toBe('string');
    });
});

